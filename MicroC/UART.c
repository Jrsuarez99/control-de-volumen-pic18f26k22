/*
 MCU: PIC18F26k22 @ 64 MHz with PLL, MCLR enabled
 Xtal: Internal-16hz
 By: Jesus Suarez -- https://www.hackster.io/
 public repository :
     | Git  - Github ... 404 =P
      |Git  - bitbucket->https://Jrsuarez99@bitbucket.org/Jrsuarez99/control-de-volumen-pic18f26k22.git
 
              ______________
             / *            \
1 Reset     |RE3      RX2/RB7| LCD-D7       28
2 R-Channel | RA0     TX2/RB6| LCD-D6       27
3 L-Channel |RA1          RB5| LCD-D5       26
4 DAC-OUT   |RA2          RB4| LCD-D4       25
5           |RA3          RB3| LCD-RS       24
6           |RA4          RB2| LCD-EN       23
7           |RA5          RB1| INT0-Encoder 22
8           |VSS          RB0| INT1-Encoder 21
9           |RA7/XTAL     VDD|              20
10          |RA6/XTAL     VSS|              19
11          |RC0       RX/RC7|              18
12          |RC1       TX/RC6|              17
13          |RC2          RC5|              16
14          |RC3          RC4|              15
            \________________/

*/
// LCD module connections
sbit LCD_RS at RB3_bit;
sbit LCD_EN at RB2_bit;
sbit LCD_D4 at RB4_bit;
sbit LCD_D5 at RB5_bit;
sbit LCD_D6 at RB6_bit;
sbit LCD_D7 at RB7_bit;

sbit LCD_RS_Direction at TRISB3_bit;
sbit LCD_EN_Direction at TRISB2_bit;
sbit LCD_D4_Direction at TRISB4_bit;
sbit LCD_D5_Direction at TRISB5_bit;
sbit LCD_D6_Direction at TRISB6_bit;
sbit LCD_D7_Direction at TRISB7_bit;
// End LCD module connections


//  Test //
int codigo,codigo2=0;
char dac=0,adc=0,adc2=0;
char vumeter=0;
char i=0;
///////////
char Canal_izq[] = "L:";
char Canal_der[] = "R:";

void _DAC_Init();
void _LCD_VM(int adc);



////////////////////////////////

//////////////////MAIN//////////////
void main()
{
///Clock Config
IRCF0_bit=1;
IRCF1_bit=0;
IRCF2_bit=1;
PLLEN_bit=1;
////////////////////////////////////////////////////////
/* Input // Output  Pin Config */
//All Digital
ANSELA=0;
ANSELB=0;
ANSELC=0;
//:TEST

TRISB0_bit=0;
//:
/////Output
//-LCD INIT
Lcd_Init();
Lcd_Cmd(_LCD_CLEAR);               // Clear display
Lcd_Cmd(_LCD_CURSOR_OFF);          // Cursor off
//-DAC 5bit Config
_DAC_Init();

/////Input
// R - Channel
TRISA0_bit=1;
ANSELA.b0=1;
// L - Channel
TRISA1_bit=1;
ANSELA.b1=1;

//Analog Input
Lcd_Out(1,1,Canal_der);
Lcd_Out(2,1,Canal_izq);
 
     while(1)
     {
     Lcd_Cmd(_LCD_CLEAR);
     Lcd_Out(1,1,Canal_der);
     Lcd_Out(2,1,Canal_izq);
     codigo=ADC_Read(0);
     codigo2=ADC_Read(1);
     // dac=codigo/32;
    //  VREFCON2= dac;
      adc=codigo/64;
      adc2=codigo2/64;
      for (i=0; i<adc; i++) 
       {
       LCD_Chr(1, 3+i,255);
       }
      for (i=0; i<adc2; i++)
      {
       LCD_Chr(2, 3+i,255);
       }
      
     Delay_ms(8);

     }// End Main While

}// End Main



void _LCD_VM()
{



}









void _DAC_Init()
{
ANSELA.b2=1;
//TRISA2_bit=0;
//Init
DACEN_bit=0;// DAC is disabled Clear
DACLPS_bit=1;//DAC Positive reference source selected
//--DAC Positive Source Select bits =  VDD
DACPSS0_bit=0;
DACPSS1_bit=0;
//--
DACEN_bit=1;// DAC is  enabled
DACOE_bit=1;
//
}
