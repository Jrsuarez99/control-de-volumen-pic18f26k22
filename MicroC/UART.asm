
_main:

;UART.c,62 :: 		void main()
;UART.c,65 :: 		IRCF0_bit=1;
	BSF         IRCF0_bit+0, BitPos(IRCF0_bit+0) 
;UART.c,66 :: 		IRCF1_bit=0;
	BCF         IRCF1_bit+0, BitPos(IRCF1_bit+0) 
;UART.c,67 :: 		IRCF2_bit=1;
	BSF         IRCF2_bit+0, BitPos(IRCF2_bit+0) 
;UART.c,68 :: 		PLLEN_bit=1;
	BSF         PLLEN_bit+0, BitPos(PLLEN_bit+0) 
;UART.c,72 :: 		ANSELA=0;
	CLRF        ANSELA+0 
;UART.c,73 :: 		ANSELB=0;
	CLRF        ANSELB+0 
;UART.c,74 :: 		ANSELC=0;
	CLRF        ANSELC+0 
;UART.c,77 :: 		TRISB0_bit=0;
	BCF         TRISB0_bit+0, BitPos(TRISB0_bit+0) 
;UART.c,81 :: 		Lcd_Init();
	CALL        _Lcd_Init+0, 0
;UART.c,82 :: 		Lcd_Cmd(_LCD_CLEAR);               // Clear display
	MOVLW       1
	MOVWF       FARG_Lcd_Cmd_out_char+0 
	CALL        _Lcd_Cmd+0, 0
;UART.c,83 :: 		Lcd_Cmd(_LCD_CURSOR_OFF);          // Cursor off
	MOVLW       12
	MOVWF       FARG_Lcd_Cmd_out_char+0 
	CALL        _Lcd_Cmd+0, 0
;UART.c,85 :: 		_DAC_Init();
	CALL        __DAC_Init+0, 0
;UART.c,89 :: 		TRISA0_bit=1;
	BSF         TRISA0_bit+0, BitPos(TRISA0_bit+0) 
;UART.c,90 :: 		ANSELA.b0=1;
	BSF         ANSELA+0, 0 
;UART.c,92 :: 		TRISA1_bit=1;
	BSF         TRISA1_bit+0, BitPos(TRISA1_bit+0) 
;UART.c,93 :: 		ANSELA.b1=1;
	BSF         ANSELA+0, 1 
;UART.c,96 :: 		Lcd_Out(1,1,Canal_der);
	MOVLW       1
	MOVWF       FARG_Lcd_Out_row+0 
	MOVLW       1
	MOVWF       FARG_Lcd_Out_column+0 
	MOVLW       _Canal_der+0
	MOVWF       FARG_Lcd_Out_text+0 
	MOVLW       hi_addr(_Canal_der+0)
	MOVWF       FARG_Lcd_Out_text+1 
	CALL        _Lcd_Out+0, 0
;UART.c,97 :: 		Lcd_Out(2,1,Canal_izq);
	MOVLW       2
	MOVWF       FARG_Lcd_Out_row+0 
	MOVLW       1
	MOVWF       FARG_Lcd_Out_column+0 
	MOVLW       _Canal_izq+0
	MOVWF       FARG_Lcd_Out_text+0 
	MOVLW       hi_addr(_Canal_izq+0)
	MOVWF       FARG_Lcd_Out_text+1 
	CALL        _Lcd_Out+0, 0
;UART.c,99 :: 		while(1)
L_main0:
;UART.c,101 :: 		Lcd_Cmd(_LCD_CLEAR);
	MOVLW       1
	MOVWF       FARG_Lcd_Cmd_out_char+0 
	CALL        _Lcd_Cmd+0, 0
;UART.c,102 :: 		Lcd_Out(1,1,Canal_der);
	MOVLW       1
	MOVWF       FARG_Lcd_Out_row+0 
	MOVLW       1
	MOVWF       FARG_Lcd_Out_column+0 
	MOVLW       _Canal_der+0
	MOVWF       FARG_Lcd_Out_text+0 
	MOVLW       hi_addr(_Canal_der+0)
	MOVWF       FARG_Lcd_Out_text+1 
	CALL        _Lcd_Out+0, 0
;UART.c,103 :: 		Lcd_Out(2,1,Canal_izq);
	MOVLW       2
	MOVWF       FARG_Lcd_Out_row+0 
	MOVLW       1
	MOVWF       FARG_Lcd_Out_column+0 
	MOVLW       _Canal_izq+0
	MOVWF       FARG_Lcd_Out_text+0 
	MOVLW       hi_addr(_Canal_izq+0)
	MOVWF       FARG_Lcd_Out_text+1 
	CALL        _Lcd_Out+0, 0
;UART.c,104 :: 		codigo=ADC_Read(0);
	CLRF        FARG_ADC_Read_channel+0 
	CALL        _ADC_Read+0, 0
	MOVF        R0, 0 
	MOVWF       _codigo+0 
	MOVF        R1, 0 
	MOVWF       _codigo+1 
;UART.c,105 :: 		codigo2=ADC_Read(1);
	MOVLW       1
	MOVWF       FARG_ADC_Read_channel+0 
	CALL        _ADC_Read+0, 0
	MOVF        R0, 0 
	MOVWF       FLOC__main+0 
	MOVF        R1, 0 
	MOVWF       FLOC__main+1 
	MOVF        FLOC__main+0, 0 
	MOVWF       _codigo2+0 
	MOVF        FLOC__main+1, 0 
	MOVWF       _codigo2+1 
;UART.c,108 :: 		adc=codigo/64;
	MOVLW       64
	MOVWF       R4 
	MOVLW       0
	MOVWF       R5 
	MOVF        _codigo+0, 0 
	MOVWF       R0 
	MOVF        _codigo+1, 0 
	MOVWF       R1 
	CALL        _Div_16x16_S+0, 0
	MOVF        R0, 0 
	MOVWF       _adc+0 
;UART.c,109 :: 		adc2=codigo2/64;
	MOVLW       64
	MOVWF       R4 
	MOVLW       0
	MOVWF       R5 
	MOVF        FLOC__main+0, 0 
	MOVWF       R0 
	MOVF        FLOC__main+1, 0 
	MOVWF       R1 
	CALL        _Div_16x16_S+0, 0
	MOVF        R0, 0 
	MOVWF       _adc2+0 
;UART.c,110 :: 		for (i=0; i<adc; i++)
	CLRF        _i+0 
L_main2:
	MOVF        _adc+0, 0 
	SUBWF       _i+0, 0 
	BTFSC       STATUS+0, 0 
	GOTO        L_main3
;UART.c,112 :: 		LCD_Chr(1, 3+i,255);
	MOVLW       1
	MOVWF       FARG_Lcd_Chr_row+0 
	MOVF        _i+0, 0 
	ADDLW       3
	MOVWF       FARG_Lcd_Chr_column+0 
	MOVLW       255
	MOVWF       FARG_Lcd_Chr_out_char+0 
	CALL        _Lcd_Chr+0, 0
;UART.c,110 :: 		for (i=0; i<adc; i++)
	INCF        _i+0, 1 
;UART.c,113 :: 		}
	GOTO        L_main2
L_main3:
;UART.c,114 :: 		for (i=0; i<adc2; i++)
	CLRF        _i+0 
L_main5:
	MOVF        _adc2+0, 0 
	SUBWF       _i+0, 0 
	BTFSC       STATUS+0, 0 
	GOTO        L_main6
;UART.c,116 :: 		LCD_Chr(2, 3+i,255);
	MOVLW       2
	MOVWF       FARG_Lcd_Chr_row+0 
	MOVF        _i+0, 0 
	ADDLW       3
	MOVWF       FARG_Lcd_Chr_column+0 
	MOVLW       255
	MOVWF       FARG_Lcd_Chr_out_char+0 
	CALL        _Lcd_Chr+0, 0
;UART.c,114 :: 		for (i=0; i<adc2; i++)
	INCF        _i+0, 1 
;UART.c,117 :: 		}
	GOTO        L_main5
L_main6:
;UART.c,119 :: 		Delay_ms(8);
	MOVLW       42
	MOVWF       R12, 0
	MOVLW       141
	MOVWF       R13, 0
L_main8:
	DECFSZ      R13, 1, 1
	BRA         L_main8
	DECFSZ      R12, 1, 1
	BRA         L_main8
	NOP
	NOP
;UART.c,121 :: 		}// End Main While
	GOTO        L_main0
;UART.c,123 :: 		}// End Main
L_end_main:
	GOTO        $+0
; end of _main

__LCD_VM:

;UART.c,127 :: 		void _LCD_VM()
;UART.c,132 :: 		}
L_end__LCD_VM:
	RETURN      0
; end of __LCD_VM

__DAC_Init:

;UART.c,142 :: 		void _DAC_Init()
;UART.c,144 :: 		ANSELA.b2=1;
	BSF         ANSELA+0, 2 
;UART.c,147 :: 		DACEN_bit=0;// DAC is disabled Clear
	BCF         DACEN_bit+0, BitPos(DACEN_bit+0) 
;UART.c,148 :: 		DACLPS_bit=1;//DAC Positive reference source selected
	BSF         DACLPS_bit+0, BitPos(DACLPS_bit+0) 
;UART.c,150 :: 		DACPSS0_bit=0;
	BCF         DACPSS0_bit+0, BitPos(DACPSS0_bit+0) 
;UART.c,151 :: 		DACPSS1_bit=0;
	BCF         DACPSS1_bit+0, BitPos(DACPSS1_bit+0) 
;UART.c,153 :: 		DACEN_bit=1;// DAC is  enabled
	BSF         DACEN_bit+0, BitPos(DACEN_bit+0) 
;UART.c,154 :: 		DACOE_bit=1;
	BSF         DACOE_bit+0, BitPos(DACOE_bit+0) 
;UART.c,156 :: 		}
L_end__DAC_Init:
	RETURN      0
; end of __DAC_Init
